#!/bin/bash
source ~/.ff_bash/install_macos.sh

if [[ "$OSTYPE" == "linux-gnu" ]]; then
  printf "Operating system not supported (Linux). Please report this issue.\n"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  # Install Homebrew
  macos_install
elif [[ "$OSTYPE" == "cygwin" ]]; then
  # POSIX compatibility layer and Linux environment emulation for Windows
  printf "Operating system not supported (cygwin). Please report this issue.\n"
elif [[ "$OSTYPE" == "msys" ]]; then
  # Lightweight shell and GNU utilities compiled for Windows (part of MinGW)
  printf "Operating system not supported (msys-mingw). Please report this issue.\n"
elif [[ "$OSTYPE" == "win32" ]]; then
  printf "Operating system not supported (Win32). Please report this issue.\n"
elif [[ "$OSTYPE" == "freebsd"* ]]; then
  printf "Operating system not supported (Freebsd). Please report this issue.\n"
else
  printf "Operating system not supported (Unknown). Please report this issue.\n"
fi

if [ -z ${ff_bashrc_version+x} ]; then 
  printf "FF Bash is not setup\n";
  printf "source $HOME/.ff_bash/.bashrc\n" >> $HOME/.bash_profile
else 
  printf "FF Bash version is '$ff_bashrc_version'\n";
fi

printf "Do you want to enable unlimited bash history (y/n)? " && read unlimited_history
touch $HOME/.ff_bash/config
case "$unlimited_history" in
  y|Y ) printf "FF_BASH_UNLIMITED_HISTORY=True\n" >> $HOME/.ff_bash/config;;
  n|N ) sed '/^FF_BASH_UNLIMITED_HISTORY.*/ d' -i $HOME/.ff_bash/config;;
  * ) printf "KILL YOURSELF!!!";;
esac

if [ "${BASH_VERSINFO:-0}" -ge 4 ]; then
  source $HOME/.bash_profile
else
  printf "Installation successful. Please close this window"
fi