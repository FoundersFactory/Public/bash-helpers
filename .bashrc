source $HOME/.ff_bash/ff_bash_version
source $HOME/.ff_bash/path_additions.sh

configfile="$HOME/.ff_bash/config"
configfile_secured="$HOME/.ff_bash/config_temp"

# check if the file contains something we don't want
if egrep -q -v '^#|^[^ ]*=[^;]*' "$configfile"; then
  echo "Config file is unclean, cleaning it..." >&2
  # filter the original to a new file
  egrep '^#|^[^ ]*=[^;&]*'  "$configfile" > "$configfile_secured"
  configfile="$configfile_secured"
fi

source $configfile

if [ FF_BASH_UNLIMITED_HISTORY = "True" ]; then
  export HISTCONTROL=erasedups
  export HISTSIZE=10000000
  shopt -s histappend
fi

gpip() {
   PIP_REQUIRE_VIRTUALENV="false" pip "$@"
}

gpip3() {
   PIP_REQUIRE_VIRTUALENV="false" pip3 "$@"
}

activate_virutal_env() {
   curDir=`pwd`
   if [ -z "$2" ]; then
     pythonVersion=3
   else
     pythonVersion=$2
   fi
   if [ ! -d "$HOME/Virtualenvs/${1}-py${pythonVersion}" ]; then
     echo "Creating new virutalenv using python $pythonVersion"
     cd "$HOME/Virtualenvs"
     virtualenv -p python${pythonVersion} ${1}-py${pythonVersion}
   fi
   cd "$HOME/Virtualenvs/${1}-py${pythonVersion}"
   source bin/activate
   cd "$curDir"
}

###AutoComplete
if [ "${BASH_VERSINFO:-0}" -ge 4 ]; then
  _activate_virutal_env() {
      local cur=${COMP_WORDS[COMP_CWORD]}
      COMPREPLY=( $(compgen -W "$(ls $HOME/Virtualenvs/ | sed -e 's/\(-py[2|3]\)*$//g' | uniq)" -- $cur) )
  }
  complete -F _activate_virutal_env activate_virutal_env
fi

if [ `command -v direnv` ]; then
  eval "$(direnv hook bash)"
fi

alias reload='source ~/.bash_profile && echo "File .bash_profile reloaded correctly" || echo "Syntax error, could not import the file"';

update_ff_bash() {
   curDir=`pwd`  
   cd "$HOME/.ff_bash"
   git pull
   cd "$curDir"
   reload
}